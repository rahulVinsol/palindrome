import Foundation

extension String {
	func isPalindrome() -> Bool {
		let midIndex: Int = count/2
		return prefix(midIndex).lowercased() == String(suffix(midIndex).reversed()).lowercased()
	}
}


print("Enter String = ", terminator: "")
let string = readLine()

switch string {
	case let str where str?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty != false:
		print("Please provide an input")
	case let str where str!.isPalindrome():
		print("Input string is a palindrome")
	default:
		print("Input string is not a palindrome")
}
